using System;

namespace Task_1
{
    class Matrix
    {     
        /// <summary>
        /// Private matrix of the class
        /// </summary>
           int[,] _matrix;

        /// <summary>
           /// These variables counters using cycles
        /// </summary>
           int _curX = 0;
           int _curY = 0;

        /// <summary>
           /// Ctor matrix of parameters
        /// </summary>
           /// <param name="matrix">array for the object type Matrix</param>
        public Matrix(int[,] matrix)
        {
            _matrix = matrix;           
        }


        /// <summary>
        /// Menorah matrix calculation on concrete element
        /// </summary>
        /// <param name="numbX">this variable is to provide an element in the range of matrix </param>
        /// <param name="numbY">this variable is to provide an element in the range of matrix</param>
        /// <returns>Value of minor</returns>
        public int GetMinorIndex(int numbX, int numbY)
        {
            int numberX;
            int numberY;

            if (numbX <= _matrix.GetLength(0) && numbY <= _matrix.GetLength(1))
            {
                numberX = numbX;
                numberY = numbY;
            }
            else
            {
                Console.WriteLine("");
                return 0;
            }

            // create a matrix for recursion
            int[,] tempMatrix = new int[_matrix.GetLength(0) - 1, _matrix.GetLength(1) - 1];


            // get elements and save to matrix minor
            for (int i = 0; i < _matrix.GetLength(1); i++)
            {
                for (int j = 0; j < _matrix.GetLength(1); j++)
                {
                    if (i == numberX || j == numberY)
                    {
                        continue;
                    }
                    else
                    {
                        tempMatrix[_curX, _curY] = _matrix[i, j];

                        _curX = _curX + 1;

                        if (_curX == tempMatrix.GetLength(0))
                        {
                            _curX = 0;
                            _curY = _curY + 1;
                        }
                    }
                }
            }

            // check algebraic additions
            if ((numberX +numberY)% 2 == 0)
            {
                return GetMatrixMinorAdd(tempMatrix);
            }
            else
            {
                return -1 * GetMatrixMinorAdd(tempMatrix);
            }

        }

        /// <summary>
        /// Matrix method found minor
        /// </summary>
        /// <param name="arr">Array </param>
        /// <returns>Value of minor</returns>
        public int GetMatrixMinorAdd(int[,] arr)
        {
            // minor matrix calculation result
            int globalDet = 0;

            // rang of currant matrix
            int matrixSize = arr.GetLength(0);


            if (matrixSize == 1 )
            {
                return 0;
            }

            if (matrixSize == 2)
            {
                return globalDet = (arr[0, 0] * arr[1, 1]) - (arr[0, 1] * arr[1, 0]);
            }

            else 
            {
                // current counter
                _curX = 0;
                _curY = 0;

                // // create a matrix for recursion
                int[,] minor = new int[matrixSize - 1, matrixSize - 1];

                //  to identify the determinants I cross off the first line ( i = 0 ) and change the column (as variable k)
                for (int k = 0; k < arr.GetLength(1); k++)
                {

                    // inner loop
                    for (int i = 0; i < arr.GetLength(0); i++)
                    {
                        for (int j = 0; j < arr.GetLength(1); j++)
                        {
                            if (i == 0 || j == k )
                            {
                                continue;
                            }

                            else
                            {
                                 minor[_curX, _curY] = arr[i,j];

                            
                                 if (_curY == minor.GetLength(1) - 1)
                                 {
                                     _curX++;
                                     _curY =0;
                                  }
                                  else
                                  {
                                      _curY++;
                                   }

                                   if (_curX == minor.GetLength(0) - 1 && _curY == minor.GetLength(1) - 1)
                                   {
                                       
                                   }
                                }
                            }
                    }
                }

                globalDet *= GetMatrixMinorAdd(minor);
            }

            return globalDet;
        }

        
        
        internal int GetMinor()
        {
            return GetMatrixMinorAdd(_matrix);
        }
    }

     class Program
    {
        /// <summary>
        /// Main () method is the starting point of the program
        /// </summary>
        /// <param name="args">The argument of the method Main ()</param>
        static void Main(string[] args)
        {
            // Initializes matrix of random numbers
            int[,] arr = new int[5, 5];
            Random rnd = new Random();

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = rnd.Next(-10, 10);
                }
            }
           
            var matrix = new Matrix(arr);

            int currant = matrix.GetMinorIndex(2,2);
            Console.WriteLine(currant);
            Console.ReadKey();
        }
    }
}